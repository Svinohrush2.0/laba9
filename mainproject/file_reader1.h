#ifndef FILE_READER1_H
#define FILE_READER1_H

#include "phone_conversations.h"

void read(const char* file_name, phone_conversations* array[], int& size);

#endif
