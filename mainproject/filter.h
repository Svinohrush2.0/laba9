#ifndef FILTER_H
#define FILTER_H

#include "phone_conversations.h"

phone_conversations** filter(phone_conversations* array[], int size, bool (*check)(phone_conversations* element), int& result_size);
bool check_tarif(phone_conversations* element);
bool check_conversations_by_date(phone_conversations* element);
#endif

