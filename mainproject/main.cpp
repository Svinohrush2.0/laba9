#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdlib>

using namespace std;

#include "phone_conversations.h"
#include "file_reader.h"
#include "constants.h"

// Процедура для преобразования в двоичную кучу поддерева с корневым узлом i, что является
// индексом в arr[]. n - размер кучи

void heapify(int arr[], int n, int i)
{
    int largest = i;
    // Инициализируем наибольший элемент как корень
    int l = 2 * i + 1; // левый = 2*i + 1
    int r = 2 * i + 2; // правый = 2*i + 2

    // Если левый дочерний элемент больше корня
    if (l < n && arr[l] < arr[largest])
        largest = l;

    // Если правый дочерний элемент больше, чем самый большой элемент на данный момент
    if (r < n && arr[r] < arr[largest])
        largest = r;

    // Если самый большой элемент не корень
    if (largest != i)
    {
        swap(arr[i], arr[largest]);

        // Рекурсивно преобразуем в двоичную кучу затронутое поддерево
        heapify(arr, n, largest);
    }
}

// Основная функция, выполняющая пирамидальную сортировку
void heapSort(int arr[], int n)
{
    // Построение кучи (перегруппируем массив)
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    // Один за другим извлекаем элементы из кучи
    for (int i = n - 1; i >= 0; i--)
    {
        // Перемещаем текущий корень в конец
        swap(arr[0], arr[i]);

        // вызываем процедуру heapify на уменьшенной куче
        heapify(arr, i, 0);
    }
}

/* Вспомогательная функция для вывода на экран массива размера n*/
void printArray(struct phone_conversations* arr[], int n)
{
    for (int i = 0; i < n; ++i)
        cout << arr[i] << " ";
    cout << "\n";
}

void quicksort(int* mas, int first, int last)
{
    int mid, count;
    int f = first, l = last;
    mid = mas[(f + l) / 2]; //вычисление опорного элемента
    do
    {
        while (mas[f] < mid) f++;
        while (mas[l] > mid) l--;
        if (f <= l) //перестановка элементов
        {
            count = mas[f];
            mas[f] = mas[l];
            mas[l] = count;
            f++;
            l--;
        }
    } while (f < l);
    if (first < l) quicksort(mas, first, l);
    if (f < last) quicksort(mas, f, last);
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "Лабораторная работа #8. GIT\n";
    cout << "Вариант #9. Телефонные разговоры\n";
    cout << "Автор: Павел Хвостюк\n";
    cout << "Группа: 11\n";
    phone_conversations* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        cout << "***** Телефонные разговоры *****\n\n";
        for (int i = 0; i < size; i++)
        {
            /********** вывод номера **********/
            cout << "Номер........: ";
            // вывод номера
            cout << subscriptions[i]->number << " ";
            cout << '\n';
            /********** вывод даты **********/
            // вывод года
            cout << "Дата выдачи.....: ";
            cout << setw(2) << setfill('0') << subscriptions[i]->date1.day << '-';
            // вывод месяца
            cout << setw(2) << setfill('0') << subscriptions[i]->date1.month << '-';
            // вывод числа
            cout << setw(2) << setfill('0') << subscriptions[i]->date1.year;
            cout << '\n';
            /********** вывод времени начала разговора **********/
            // вывод часа
            cout << "Время начала разговора...: ";
            cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
            // вывод минут
            cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
            // вывод секунд
            cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
            cout << '\n';
            /********** вывод продолжительности разговора **********/
            // вывод часа
            cout << "Продолжительность разговора...: ";
            cout << setw(2) << setfill('0') << subscriptions[i]->longs.hour << ':';
            // вывод минут
            cout << setw(2) << setfill('0') << subscriptions[i]->longs.minute << ':';
            // вывод секунд
            cout << setw(2) << setfill('0') << subscriptions[i]->longs.second;
            cout << '\n';
            /********** вывод тарифа **********/
            cout << "Тариф........: ";
            // вывод тарифа
            cout << subscriptions[i]->tarif << " ";
            cout << '\n';
            /********** вывод стоимости **********/
            cout << "Стоимость 1 минуты разговора........: ";
            // вывод стоимости
            cout << subscriptions[i]->cost << " ";
            cout << '\n';
            cout << '\n';
        }
        cout << "Вывод отсортированных по тарифу разговоров: " << endl;
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            if (strcmp(subscriptions[i]->tarif, "мобильный") == 0) {
                /********** вывод номера **********/
                cout << "Номер........: ";
                // вывод номера
                cout << subscriptions[i]->number << " ";
                cout << '\n';
                /********** вывод даты **********/
                // вывод года
                cout << "Дата выдачи.....: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.day << '-';
                // вывод месяца
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.month << '-';
                // вывод числа
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.year;
                cout << '\n';
                /********** вывод времени начала разговора **********/
                // вывод часа
                cout << "Время начала разговора...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
                // вывод минут
                cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
                // вывод секунд
                cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
                cout << '\n';
                /********** вывод продолжительности разговора **********/
                // вывод часа
                cout << "Продолжительность разговора...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.hour << ':';
                // вывод минут
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.minute << ':';
                // вывод секунд
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.second;
                cout << '\n';
                /********** вывод тарифа **********/
                cout << "Тариф........: ";
                // вывод тарифа
                cout << subscriptions[i]->tarif << " ";
                cout << '\n';
                /********** вывод стоимости **********/
                cout << "Стоимость 1 минуты разговора........: ";
                // вывод стоимости
                cout << subscriptions[i]->cost << " ";
                cout << '\n';
                cout << '\n';
            }
        }
        cout << "Вывод отсортированных по ноябрю 21 года разговоров: " << endl;
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            if (subscriptions[i]->date1.month == 11 && subscriptions[i]->date1.year == 21) {
                /********** вывод номера **********/
                cout << "Номер........: ";
                // вывод номера
                cout << subscriptions[i]->number << " ";
                cout << '\n';
                /********** вывод даты **********/
                // вывод года
                cout << "Дата выдачи.....: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.day << '-';
                // вывод месяца
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.month << '-';
                // вывод числа
                cout << setw(2) << setfill('0') << subscriptions[i]->date1.year;
                cout << '\n';
                /********** вывод времени начала разговора **********/
                // вывод часа
                cout << "Время начала разговора...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->start.hour << ':';
                // вывод минут
                cout << setw(2) << setfill('0') << subscriptions[i]->start.minute << ':';
                // вывод секунд
                cout << setw(2) << setfill('0') << subscriptions[i]->start.second;
                cout << '\n';
                /********** вывод продолжительности разговора **********/
                // вывод часа
                cout << "Продолжительность разговора...: ";
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.hour << ':';
                // вывод минут
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.minute << ':';
                // вывод секунд
                cout << setw(2) << setfill('0') << subscriptions[i]->longs.second;
                cout << '\n';
                /********** вывод тарифа **********/
                cout << "Тариф........: ";
                // вывод тарифа
                cout << subscriptions[i]->tarif << " ";
                cout << '\n';
                /********** вывод стоимости **********/
                cout << "Стоимость 1 минуты разговора........: ";
                // вывод стоимости
                cout << subscriptions[i]->cost << " ";
                cout << '\n';
                cout << '\n';
            }
        }
        int* a = new int[size];
        for (int i = 0; i < size; i++)
        {
            int k = subscriptions[i]->longs.hour * 3600 + subscriptions[i]->longs.minute * 60 + subscriptions[i]->longs.second;
            cout << k << endl;
            a[i] = k;
        }
        heapSort(a, size);
        cout << '\n';
        for (int i = 0; i < size; i++)
        {
            cout << a[i] << endl;
            cout << '\n';
            for (int k = 0; k < size; k++) {
                if (subscriptions[k]->longs.hour * 3600 + subscriptions[k]->longs.minute * 60 + subscriptions[k]->longs.second == a[i]) {
                    /********** вывод номера **********/
                    cout << "Номер........: ";
                    // вывод номера
                    cout << subscriptions[k]->number << " ";
                    cout << '\n';
                    /********** вывод даты **********/
                    // вывод года
                    cout << "Дата выдачи.....: ";
                    cout << setw(2) << setfill('0') << subscriptions[k]->date1.day << '-';
                    // вывод месяца
                    cout << setw(2) << setfill('0') << subscriptions[k]->date1.month << '-';
                    // вывод числа
                    cout << setw(2) << setfill('0') << subscriptions[k]->date1.year;
                    cout << '\n';
                    /********** вывод времени начала разговора **********/
                    // вывод часа
                    cout << "Время начала разговора...: ";
                    cout << setw(2) << setfill('0') << subscriptions[k]->start.hour << ':';
                    // вывод минут
                    cout << setw(2) << setfill('0') << subscriptions[k]->start.minute << ':';
                    // вывод секунд
                    cout << setw(2) << setfill('0') << subscriptions[k]->start.second;
                    cout << '\n';
                    /********** вывод продолжительности разговора **********/
                    // вывод часа
                    cout << "Продолжительность разговора...: ";
                    cout << setw(2) << setfill('0') << subscriptions[k]->longs.hour << ':';
                    // вывод минут
                    cout << setw(2) << setfill('0') << subscriptions[k]->longs.minute << ':';
                    // вывод секунд
                    cout << setw(2) << setfill('0') << subscriptions[k]->longs.second;
                    cout << '\n';
                    /********** вывод тарифа **********/
                    cout << "Тариф........: ";
                    // вывод тарифа
                    cout << subscriptions[k]->tarif << " ";
                    cout << '\n';
                    /********** вывод стоимости **********/
                    cout << "Стоимость 1 минуты разговора........: ";
                    // вывод стоимости
                    cout << subscriptions[k]->cost << " ";
                    cout << '\n';
                    cout << '\n';
                }
            }
        }
        cout << "Разговоры отсортрованные по номеру: " << endl;
        cout << '\n';
        int* c = new int[size];
        int* c1 = new int[size];
        for (int i = 0; i < size; i++)
        {
            int v = 1000000;
            int y = 0;
            if ((subscriptions[i]->number)[2] == '3') {
                for (int k = 4; k < strlen(subscriptions[k]->number); k++) {
                    char m = (subscriptions[i]->number)[k];
                    int n = ((int)m - '0') * v;
                    y += n;
                    c[i] = y;
                    c1[i] = y;
                    v /= 10;
                }
            }
        }
        cout << '\n';
        int* b = new int[size];
        int* b1 = new int[size];
        for (int i = 0; i < size; i++)
        {
            int v = 1000000;
            int y = 0;
            if ((subscriptions[i]->number)[2] == '2') {
                for (int k = 4; k < strlen(subscriptions[k]->number); k++) {
                    char m = (subscriptions[i]->number)[k];
                    int n = ((int)m - '0') * v;
                    y += n;
                    b[i] = y;
                    b1[i] = y;
                    v /= 10;
                }
            }
        }
        quicksort(c, 0, size - 1);
        quicksort(b, 0, size - 1);
        for (int i = 0; i < size; i++) {
            if (b[i] > 0)
                cout << b[i] << endl;
        }
        cout << '\n';
        for (int i = 0; i < size; i++) {
            if (b[i] > 0) {
                for (int k = 0; k < size; k++) {
                    if (b[i] == b1[k]) {
                        /********** вывод номера **********/
                        cout << "Номер........: ";
                        // вывод номера
                        cout << subscriptions[k]->number << " ";
                        cout << '\n';
                        /********** вывод даты **********/
                        // вывод года
                        cout << "Дата выдачи.....: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.day << '-';
                        // вывод месяца
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.month << '-';
                        // вывод числа
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.year;
                        cout << '\n';
                        /********** вывод времени начала разговора **********/
                        // вывод часа
                        cout << "Время начала разговора...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.hour << ':';
                        // вывод минут
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.minute << ':';
                        // вывод секунд
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.second;
                        cout << '\n';
                        /********** вывод продолжительности разговора **********/
                        // вывод часа
                        cout << "Продолжительность разговора...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.hour << ':';
                        // вывод минут
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.minute << ':';
                        // вывод секунд
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.second;
                        cout << '\n';
                        /********** вывод тарифа **********/
                        cout << "Тариф........: ";
                        // вывод тарифа
                        cout << subscriptions[k]->tarif << " ";
                        cout << '\n';
                        /********** вывод стоимости **********/
                        cout << "Стоимость 1 минуты разговора........: ";
                        // вывод стоимости
                        cout << subscriptions[k]->cost << " ";
                        cout << '\n';
                        cout << '\n';
                    }
                }
            }
        }
        cout << '\n';
        for (int i = 0; i < size; i++) {
            if (c[i] > 0)
                cout << c[i] << endl;
        }
        cout << '\n';
        for (int i = 0; i < size; i++) {
            if (c[i] > 0) {
                for (int k = 0; k < size; k++) {
                    if (c[i] == c1[k]) {
                        /********** вывод номера **********/
                        cout << "Номер........: ";
                        // вывод номера
                        cout << subscriptions[k]->number << " ";
                        cout << '\n';
                        /********** вывод даты **********/
                        // вывод года
                        cout << "Дата выдачи.....: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.day << '-';
                        // вывод месяца
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.month << '-';
                        // вывод числа
                        cout << setw(2) << setfill('0') << subscriptions[k]->date1.year;
                        cout << '\n';
                        /********** вывод времени начала разговора **********/
                        // вывод часа
                        cout << "Время начала разговора...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.hour << ':';
                        // вывод минут
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.minute << ':';
                        // вывод секунд
                        cout << setw(2) << setfill('0') << subscriptions[k]->start.second;
                        cout << '\n';
                        /********** вывод продолжительности разговора **********/
                        // вывод часа
                        cout << "Продолжительность разговора...: ";
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.hour << ':';
                        // вывод минут
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.minute << ':';
                        // вывод секунд
                        cout << setw(2) << setfill('0') << subscriptions[k]->longs.second;
                        cout << '\n';
                        /********** вывод тарифа **********/
                        cout << "Тариф........: ";
                        // вывод тарифа
                        cout << subscriptions[k]->tarif << " ";
                        cout << '\n';
                        /********** вывод стоимости **********/
                        cout << "Стоимость 1 минуты разговора........: ";
                        // вывод стоимости
                        cout << subscriptions[k]->cost << " ";
                        cout << '\n';
                        cout << '\n';
                    }
                }
            }
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}