#include "filter.h"
#include <cstring>
#include <iostream>

phone_conversations** filter(phone_conversations* array[], int size, bool (*check)(phone_conversations* element), int& result_size)
{
	phone_conversations** result = new phone_conversations * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_tarif(phone_conversations* element)
{
	return strcmp(element->tarif, "мобильный") == 0;
}

bool check_conversations_by_date(phone_conversations* element)
{
	return element->date1.month == 11 && element->date1.year == 21;
}
