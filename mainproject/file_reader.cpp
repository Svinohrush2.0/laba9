#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.year = atoi(str_number);
    return result;
}

time1 convert1(char* str)
{
    time1 result;
    char* context = NULL;
    char* str_number = strtok_s(str, ":", &context);
    result.hour = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.minute = atoi(str_number);
    str_number = strtok_s(NULL, ":", &context);
    result.second = atoi(str_number);
    return result;
}

void read(const char* file_name, struct phone_conversations* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            phone_conversations* item = new phone_conversations;
            file >> item->number;
            file >> tmp_buffer;
            item->date1 = convert(tmp_buffer);
            file >> tmp_buffer;
            item->start = convert1(tmp_buffer);
            file >> tmp_buffer;
            item->longs = convert1(tmp_buffer);
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file >> item->tarif;
            file >> item->cost;
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}